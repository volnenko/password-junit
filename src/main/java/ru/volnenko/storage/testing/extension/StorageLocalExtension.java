package ru.volnenko.storage.testing.extension;

import org.junit.jupiter.api.extension.ConditionEvaluationResult;
import org.junit.jupiter.api.extension.ExecutionCondition;
import org.junit.jupiter.api.extension.ExtensionContext;
import ru.volnenko.storage.testing.inject.StorageInjector;

import java.util.Optional;

public class StorageLocalExtension implements ExecutionCondition {

    @Override
    public ConditionEvaluationResult evaluateExecutionCondition(ExtensionContext extensionContext) {
        final Optional<Object> optional = extensionContext.getTestInstance();
        final Object testInstance = optional.orElse(null);
        StorageInjector.inject(testInstance);
        return ConditionEvaluationResult.enabled("StorageLocal");
    }

}
