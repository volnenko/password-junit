package ru.volnenko.storage.testing.rule;

import lombok.SneakyThrows;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import ru.volnenko.storage.testing.inject.StorageInjector;

public class StorageLocalRule implements TestRule {

    @SneakyThrows
    public StorageLocalRule(Object test) {
        StorageInjector.inject(test);
    }

    @Override
    public Statement apply(Statement statement, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                statement.evaluate();
            }
        };
    }

}
