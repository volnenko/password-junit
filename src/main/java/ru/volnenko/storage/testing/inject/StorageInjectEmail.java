package ru.volnenko.storage.testing.inject;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.volnenko.password.storage.StorageLocal;
import ru.volnenko.password.storage.StoragePassword;
import ru.volnenko.storage.testing.annotation.PwdEmail;

import java.lang.reflect.Field;

class StorageInjectEmail implements StorageInject {

    @Override
    @SneakyThrows
    public boolean inject(@NotNull Field field, @NotNull Object object) {
        if (!field.isAnnotationPresent(PwdEmail.class)) return false;
        if (!field.getType().isAssignableFrom(String.class)) return false;
        final PwdEmail pwdEmail = field.getAnnotation(PwdEmail.class);
        final String serviceName = pwdEmail.service();
        final StoragePassword value = StorageLocal.global().findOneByService(serviceName);
        if (value == null) return false;
        field.setAccessible(true);
        field.set(object, value.email());
        return true;
    }

}
