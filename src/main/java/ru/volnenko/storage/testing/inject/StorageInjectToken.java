package ru.volnenko.storage.testing.inject;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.volnenko.password.storage.StorageLocal;
import ru.volnenko.password.storage.StoragePassword;
import ru.volnenko.storage.testing.annotation.PwdToken;

import java.lang.reflect.Field;

class StorageInjectToken implements StorageInject {

    @Override
    @SneakyThrows
    public boolean inject(@NotNull final Field field, @NotNull final Object object) {
        if (!field.isAnnotationPresent(PwdToken.class)) return false;
        if (!field.getType().isAssignableFrom(String.class)) return false;
        final PwdToken pwdToken = field.getAnnotation(PwdToken.class);
        final String serviceName = pwdToken.service();
        final StoragePassword value = StorageLocal.global().findOneByService(serviceName);
        if (value == null) return false;
        field.setAccessible(true);
        field.set(object, value.token());
        return true;
    }

}
