package ru.volnenko.storage.testing.inject;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;

interface StorageInject {

    boolean inject(@NotNull Field field, @NotNull Object object);

}
