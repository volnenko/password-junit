package ru.volnenko.storage.testing.inject;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Field;

public class StorageInjector {

    private static final StorageInject[] INJECTORS = new StorageInject[] {
            new StorageInjectLogin(), new StorageInjectUrl(), new StorageInjectPassword(),
            new StorageInjectHost(), new StorageInjectToken(), new StorageInjectName(),
            new StorageInjectEmail()
    };

    public static void inject(@Nullable final Object object) {
        if (object == null) return;
        @NotNull final Field[] fields = object.getClass().getDeclaredFields();
        for (@NotNull final StorageInject storageInject: INJECTORS) {
            for (@NotNull final Field field : fields) {
                storageInject.inject(field, object);
            }
        }
    }

}
