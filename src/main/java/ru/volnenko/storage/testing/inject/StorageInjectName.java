package ru.volnenko.storage.testing.inject;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.volnenko.password.storage.StorageLocal;
import ru.volnenko.password.storage.StoragePassword;
import ru.volnenko.storage.testing.annotation.PwdName;

import java.lang.reflect.Field;

class StorageInjectName implements StorageInject {

    @Override
    @SneakyThrows
    public boolean inject(@NotNull final Field field, @NotNull final Object object) {
        if (!field.isAnnotationPresent(PwdName.class)) return false;
        if (!field.getType().isAssignableFrom(String.class)) return false;
        final PwdName pwdName = field.getAnnotation(PwdName.class);
        final String serviceName = pwdName.service();
        final StoragePassword value = StorageLocal.global().findOneByService(serviceName);
        if (value == null) return false;
        field.setAccessible(true);
        field.set(object, value.name());
        return true;
    }

}
