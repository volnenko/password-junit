package ru.volnenko.storage.testing.inject;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.volnenko.password.storage.StorageLocal;
import ru.volnenko.password.storage.StoragePassword;
import ru.volnenko.storage.testing.annotation.PwdURL;

import java.lang.reflect.Field;

public class StorageInjectUrl implements StorageInject {

    @Override
    @SneakyThrows
    public boolean inject(@NotNull Field field, @NotNull Object object) {
        if (!field.isAnnotationPresent(PwdURL.class)) return false;
        if (!field.getType().isAssignableFrom(String.class)) return false;
        final PwdURL pwdURL = field.getAnnotation(PwdURL.class);
        final String serviceName = pwdURL.service();
        final StoragePassword value = StorageLocal.global().findOneByService(serviceName);
        if (value == null) return false;
        field.setAccessible(true);
        field.set(object, value.url());
        return true;
    }

}
