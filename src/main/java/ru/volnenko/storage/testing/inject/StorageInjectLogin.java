package ru.volnenko.storage.testing.inject;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.volnenko.password.storage.StorageLocal;
import ru.volnenko.password.storage.StoragePassword;
import ru.volnenko.storage.testing.annotation.PwdLogin;

import java.lang.reflect.Field;

public class StorageInjectLogin implements StorageInject {

    @Override
    @SneakyThrows
    public boolean inject(@NotNull Field field, @NotNull Object object) {
        if (!field.isAnnotationPresent(PwdLogin.class)) return false;
        if (!field.getType().isAssignableFrom(String.class)) return false;
        final PwdLogin pwdLogin = field.getAnnotation(PwdLogin.class);
        final String serviceName = pwdLogin.service();
        final StoragePassword value = StorageLocal.global().findOneByService(serviceName);
        if (value == null) return false;
        field.setAccessible(true);
        field.set(object, value.login());
        return true;
    }

}
