package ru.volnenko.storage.testing.inject;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.volnenko.password.storage.StorageLocal;
import ru.volnenko.password.storage.StoragePassword;
import ru.volnenko.storage.testing.annotation.PwdPassword;

import java.lang.reflect.Field;

public class StorageInjectPassword implements StorageInject {

    @Override
    @SneakyThrows
    public boolean inject(@NotNull Field field, @NotNull Object object) {
        if (!field.isAnnotationPresent(PwdPassword.class)) return false;
        if (!field.getType().isAssignableFrom(String.class)) return false;
        final PwdPassword pwdPassword = field.getAnnotation(PwdPassword.class);
        final String serviceName = pwdPassword.service();
        final StoragePassword value = StorageLocal.global().findOneByService(serviceName);
        if (value == null) return false;
        field.setAccessible(true);
        field.set(object, value.password());
        return true;
    }

}
