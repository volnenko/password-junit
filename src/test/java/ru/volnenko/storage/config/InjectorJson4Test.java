package ru.volnenko.storage.config;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import ru.volnenko.storage.testing.annotation.PwdLogin;
import ru.volnenko.storage.testing.annotation.PwdPassword;
import ru.volnenko.storage.testing.rule.StorageLocalRule;

public class InjectorJson4Test {

    @Rule
    public StorageLocalRule storageLocalRule = new StorageLocalRule(this);

    @PwdLogin(service = "app-1")
    private String login;

    @PwdPassword(service = "app-1")
    private String password;

    @Test
    public void test() {
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
    }

}
