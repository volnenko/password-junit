package ru.volnenko.storage.config;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import ru.volnenko.storage.testing.annotation.PwdHost;
import ru.volnenko.storage.testing.annotation.PwdLogin;
import ru.volnenko.storage.testing.annotation.PwdPassword;
import ru.volnenko.storage.testing.annotation.PwdURL;
import ru.volnenko.storage.testing.extension.StorageLocalExtension;

@ExtendWith({StorageLocalExtension.class})
public class InjectorJson5Test {

    @PwdLogin(service = "app-1")
    private String login;

    @PwdPassword(service = "app-1")
    private String password;

    @PwdURL(service = "app-1")
    private String url;

    @PwdHost(service = "app-1")
    private String host;

    @Test
    public void test() {
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        Assert.assertNotNull(host);
        Assert.assertNotNull(url);
    }

}
