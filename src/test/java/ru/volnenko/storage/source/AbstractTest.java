package ru.volnenko.storage.source;

import ru.volnenko.password.storage.StorageLocal;

public abstract class AbstractTest {

    public AbstractTest() {
        StorageLocal.global().clear()
                .storage().withService("app-1").login("demo").password("1234")
                .storage().withService("app-2").login("admin").password("4567");
    }

}
